package br.ucsal.bes20221.bd2.clinica;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Exemplo {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("clinicadb2");
        EntityManager em = emf.createEntityManager();

        UF uf = new UF("BA", "Bahia");

        em.getTransaction().begin();
        em.persist(uf);
        em.getTransaction().commit();

        System.out.println("em.contains(aluno)=" + em.contains(uf));
        emf.close();
    }

}
