-- Function:
create or replace function f_validacao_data_vigencia()
returns trigger 
as 
$$
declare 
    v_data_inicio date;

    begin 
        
        select
            data_inicio_vigencia into v_data_inicio
        from 
            diaria d
        where 
            ((d.data_inicio_vigencia >= NEW.data_inicio_vigencia) and (d.data_inicio_vigencia >= NEW.data_fim_vigencia)
        or 
            (d.data_fim_vigencia <= NEW.data_fim_vigencia) and (d.data_fim_vigencia <= NEW.data_inicio_vigencia))
        or 
            d.codigo_grupo <> NEW.codigo_grupo;  
        
        if v_data_inicio is not null then
            return new;
        end if;
    
        return null;
    end;
$$
LANGUAGE PLPGSQL;

-- Trigger
create or replace trigger trg_validacao_data_vigencia
before insert 
on diaria
for each row 
execute procedure f_validacao_data_vigencia();