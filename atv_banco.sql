create table audit_grupo (
  operation varchar(20) not null,
  username varchar(40) not null,
  datetime timestamp not null,
  states varchar(200)
);

create or replace function f_audit_grupo()
returns trigger 
as 
$$
declare 
    v_states varchar(200);
	v_user varchar(100);
	v_timestamp timestamp;
    begin 
		select current_user
		into v_user;
		
		select current_timestamp
		into v_timestamp;
		
        IF TG_OP = 'INSERT' THEN
        	v_states := concat('new(', new.codigo, ', ', new.nome, ')');
			insert into 
        	audit_grupo (operation, username, datetime, states)
            values
  			(TG_OP, v_user, v_timestamp, v_states);
			
			return new;
        ELSIF TG_OP = 'DELETE' THEN
        	v_states := concat('old(', old.codigo, ', ', old.nome, ')');
			insert into 
        	audit_grupo (operation, username, datetime, states)
            values
  			(TG_OP, v_user, v_timestamp, v_states);
			
			return old;
        ELSIF TG_OP = 'UPDATE' THEN
        	v_states := concat('new(', new.codigo, ', ', new.nome, ') old(', old.codigo, ', ', old.nome, ')');
			insert into 
        	audit_grupo (operation, username, datetime, states)
            values
  			(TG_OP, v_user, v_timestamp, v_states);
			
			return new;
        END IF;
        
        
    end;
$$
LANGUAGE PLPGSQL;

create trigger trg_audit_grupo
before insert OR delete OR update
on grupo
for each row 
execute procedure f_audit_grupo();

create view v_audit_grupo
as
select operation as OPERACAO, username as USUARIO, datetime as DATA_OPERACAO, states as ESTADOS from audit_grupo 