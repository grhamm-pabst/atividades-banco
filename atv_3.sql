create table proposta_matricula (
	matricula_aluno 		integer	not null,
	cod_disciplina		char(6)	not null);
create table aluno (
	matricula		serial			not null,
	nome			varchar(40)		not null,
	telefone		varchar(11)		);
create table disciplina (
	cod			char(6)		not null,
	nome			varchar(100)	not null,
	carga_horaria		smallint	not null,
	matricula_professor	integer	not null);
create table professor (
	matricula		serial			not null,
	nome			varchar(40)		not null,
	data_admissao	date			not null,
	email			varchar(250)		);
    
alter table proposta_matricula add constraint matricula_aluno_proposta_matricula_pk PRIMARY KEY (matricula_aluno);

alter table aluno add constraint matricula_aluno_pk PRIMARY KEY (matricula);

alter table disciplina add constraint cod_disciplina_pk PRIMARY KEY (cod);

alter table professor add constraint matricula_professor_pk PRIMARY KEY (matricula);

alter table disciplina add constraint carga_horaria_chk CHECK (carga_horaria = 30 OR carga_horaria = 60);

alter table disciplina add constraint matricula_professor_disciplina_fk foreign key (matricula_professor) references professor (matricula);

alter table proposta_matricula add constraint matricula_aluno_proposta_matricula foreign key (matricula_aluno) references aluno (matricula);

alter table proposta_matricula add constraint cod_disciplina_proposta_matricula foreign key (cod_disciplina) references disciplina (cod);

insert into professor (matricula, nome, data_admissao, email) values (1, 'Gean', '2000-12-31', 'reptossauro123@gmail.com');

insert into disciplina (cod, nome, carga_horaria, matricula_professor) values (1, 'Geans class', 30, 1);

insert into professor (matricula, nome, data_admissao, email) values (2, 'Ana', '2000-12-31', 'ana@gmail.com');

insert into disciplina (cod, nome, carga_horaria, matricula_professor) values (2, 'Anas class', 60, 2);

insert into professor (matricula, nome, data_admissao, email) values (3, 'Bia', '2000-12-31', 'bia@gmail.com');

create view v_professor_disciplina as
select p.nome, p.email, d.cod as codigo_disciplina, coalesce(d.nome, '(nenhuma disciplina associada)') as nome_disciplina
from disciplina d
right join professor p on (d.matricula_professor = p.matricula)
order by p.nome, d.nome asc;

select * from v_professor_disciplina;

select nome, data_admissao
from professor
where matricula not in (select matricula_professor from disciplina)
order by nome asc;
